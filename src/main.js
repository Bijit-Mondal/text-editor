import { createApp } from 'vue'
import App from './App.vue'
import PrimeVue from 'primevue/config';

import 'primevue/resources/themes/aura-dark-purple/theme.css'       //theme

const app = createApp(App)
app.use(PrimeVue);
app.mount('#app');
